import React from "react";
import logo from "./logo.svg";
import "./App.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import Login from "./component/Login/Login";
import { Col, Container, Row } from "react-bootstrap";

function App() {
  return (
    <div className="App" >
      <Container>
        <Row className="justify-content-md-center">
          <Col md={{ span: 4, offset: -1 }} className="mt-3 shadow-lg p-3 mb-5 bg-white rounded"><Login /></Col>
        </Row>
      </Container>
      
    </div>
  );
}

export default App;
